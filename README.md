# Juliet

Juliet is a minimal application template for your projects. It gives you
a directory structure, and a place for your code and data.

To use Juliet for a new project called "Hello", simply:

 1. Check out the Juliet project from Git.
 2. Create a Hello directory in lib/julia.
 3. In that directory, create a Hello.jl module with an __init__ function.
 4. Symlink lib/julia/Juliet/main.jl to /usr/local/bin/hello, for example.

That's it. Now you can run "hello" from any directory and it will set up
the path to include your project's Hello directory, and your own
initialization code will launch your project (once you write it).

Juliet is based loosely around SaturnV, a better launcher:
https://gitlab.com/AppliedNeuralNetworks/saturnv.git
