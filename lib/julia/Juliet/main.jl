#!/usr/bin/julia

module Juliet

function __init__()
    package_name = ucfirst(basename(@__FILE__))
    julia_library_path = dirname(dirname(realpath(@__FILE__)))

    push!(LOAD_PATH, string(julia_library_path, "/", package_name))
    push!(LOAD_PATH, string(julia_library_path, "/Juliet"))

    eval(Expr(:import, symbol(package_name)))
end

end # module Juliet

# vim:ft=julia
